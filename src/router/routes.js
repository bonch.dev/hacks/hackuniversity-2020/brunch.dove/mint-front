const routes = [
  {
    path: '/',
    component: () => import('../layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('../pages/Main.vue') }
    ]
  },
  {
    path: '/',
    component: () => import('../layouts/PagesLayout.vue'),
    children: [
      { path: 'articles', component: () => import('pages/Articles.vue') },
      {
        path: 'article',
        component: () => import('pages/PageArticle.vue'),
        children: [
          {
            name: 'article.page',
            path: '/article/:id',
            component: () => import('pages/PageArticle.vue')
          }
        ]
      },
      { path: 'events', component: () => import('pages/Events.vue') },
      { path: 'profiles', component: () => import('pages/Profiles.vue') },
      {
        path: 'profile',
        component: () => import('pages/PageProfile.vue'),
        children: [
          {
            name: 'profile.page',
            path: '/profile/:id',
            component: () => import('pages/PageProfile.vue')
          }
        ]
      }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
