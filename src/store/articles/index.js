import api from '../../boot/api'

const state = {
  articles: null
}

const getters = {
  articles: state => state.articles
}

const mutations = {
  GET_ARTICLES (state, payload) {
    state.articles = payload
  }
}

const actions = {
  getArticles ({ commit }) {
    return new Promise((resolve, reject) => {
      api.get('')
        .then((response) => {
          commit('GET_ARTICLES', response)
          resolve(response)
        })
        .catch((error) => {
          reject(error)
        })
    })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
