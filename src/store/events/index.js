import api from '../../boot/api'

const state = {
  events: null
}

const getters = {
  events: state => state.events
}

const mutations = {
  GET_EVENTS (state, payload) {
    state.events = payload
  }
}

const actions = {
  getEvents ({ commit }) {
    return new Promise((resolve, reject) => {
      api.get('')
        .then((response) => {
          commit('GET_EVENTS', response)
          resolve(response)
        })
        .catch((error) => {
          reject(error)
        })
    })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
