import api from '../../boot/api'

const state = {
  profiles: null,
  idUser: 7,
  requests: null
}

const getters = {
  profiles: state => state.profiles,
  requests: state => state.requests
}

const mutations = {
  GET_USERS (state, payload) {
    state.profiles = payload
  },
  GET_REQUESTS (state, payload) {
    state.requests = payload
  }
}

const actions = {
  getUsers ({ commit }) {
    return new Promise((resolve, reject) => {
      api.get('users')
        .then((response) => {
          commit('GET_USERS', response)
          resolve(response)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  getRequests ({ commit }) {
    return new Promise((resolve, reject) => {
      api.get('works')
        .then((response) => {
          commit('GET_REQUESTS', response.data.data)
          resolve(response)
        })
        .catch((error) => {
          reject(error)
        })
    })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
