// import API_URL from '../../boot/api'

const state = {
  userId: null,
  isAuth: false
}

const getters = {
  userId: state => state.userId,
  isAuth: state => state.isAuth
}

const mutations = {
  LOGIN (state, payload) {
    state.userId = payload
    state.isAuth = true
  },
  LOGOUT (state) {
    state.userId = null
    state.isAuth = false
  }
}

const actions = {
  login ({ commit }) {
    commit('LOGIN', 2)
  },
  logout ({ commit }) {
    commit('LOGOUT')
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
