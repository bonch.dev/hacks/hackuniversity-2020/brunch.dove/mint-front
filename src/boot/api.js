import axios from 'axios'
export const BASE_URL = 'https://17612898-review-mintt-dpj1me.server.bonch.dev/'
export const API_URL = BASE_URL + 'api/'

const api = axios.create({
  baseURL: API_URL
})

export default api
